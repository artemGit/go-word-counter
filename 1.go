package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"
	"sync/atomic"
)

func main() {
	var wg sync.WaitGroup
	var total int32

	hc := make(chan struct{}, 5)
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		wg.Add(1)
		hc <- struct{}{}

		go handle(scanner.Text(), &total, &wg, hc)
	}

	checkError(scanner.Err())

	wg.Wait()

	fmt.Printf("Total: %d\n", atomic.LoadInt32(&total))
}


func handle(url string, total *int32, wg *sync.WaitGroup, hc chan struct{}) {
	defer wg.Done()
	count := bytes.Count(getUrlBody(url), []byte("Go"))

	fmt.Printf("Count for %s: %d\n", url, count)

	atomic.AddInt32(total, int32(count))

	<- hc
}

func getUrlBody(url string) []byte {
	resp, err := http.Get(url)
	checkError(err)

	body, err := ioutil.ReadAll(resp.Body)
	checkError(err)

	return body
}

func checkError(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}
